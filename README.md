# configs

## Installation

Install `git` and `stow`

```
stow */
```

### Language servers

```
$ pipx install python-lsp-server
$ pipx inject python-lsp-server pylsp-mypy
$ pipx inject python-lsp-server pyls-isort

$ go install golang.org/x/tools/gopls@latest

$ raco pkg install racket-langserver

# npm i -g vscode-langservers-extracted  # html and css

# npm i -g typescript typescript-language-server

$ rustup component add rls rust-analysis rust-src

$ npm install -g vim-language-server

$ yay -S clojure-lsp-bin
OR
$ brew install clojure-lsp/brew/clojure-lsp-native
```

### Audio interface stereo -> Mono

#### Pulse audio

Get the name of the the device with
```
$ pacmd list-sources | grep -e 'index:' -e device.string -e 'name:'
```
Add the following to `/etc/pulse/default.pa`, swapping out the name after `master` for what you got:

```
load-module module-remap-source master=alsa_input.usb-Avid_Fast_Track_Solo_0217127E000C2400-00.analog-stereo channels=1 master_channel_map=front-right channel_map=mono source_properties="device.description='Avid Instrument'"
load-module module-remap-source master=alsa_input.usb-Avid_Fast_Track_Solo_0217127E000C2400-00.analog-stereo channels=1 master_channel_map=front-left channel_map=mono source_properties="device.description='Avid Mic'"
```

#### Pipewire

There's a pipewire folder in here with a thing in it.

Run `$ wpctl status` to get the device id, then `$ wpctl inspect <id>` to get the `node.name` to use as the `target.object`
