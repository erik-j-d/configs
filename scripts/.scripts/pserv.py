#!/usr/bin/env python3

import sys
from http.server import HTTPServer, SimpleHTTPRequestHandler, test


class CORSRequestHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header("Access-Control-Allow-Origin", "*")
        SimpleHTTPRequestHandler.end_headers(self)


if __name__ == "__main__":
    port = int(sys.argv[1]) if len(sys.argv) > 1 else 8000
    is_provided_port = bool(len(sys.argv) > 1)
    attempts = 0
    while True and attempts < 100:
        try:
            test(
                CORSRequestHandler,
                HTTPServer,
                port=port,
                bind="localhost",
            )
        except OSError as e:
            if not is_provided_port:
                print(f"Port {port} taken, trying port {port+1}...")
                port += 1
                attempts += 1
            else:
                raise e

    sys.exit(1)
