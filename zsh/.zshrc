# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Per computer extra settings
test -e "${HOME}/.zsh_${HOST}" && source "${HOME}/.zsh_${HOST}"
test -e "${HOME}/.zsh_this" && source "${HOME}/.zsh_this"

. ~/.zsh_aliases
. ~/.zsh_functions
. ~/.zsh_environment
